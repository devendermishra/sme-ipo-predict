#!/usr/bin/python3

import pickle

import pandas as pd
from sklearn.ensemble.bagging import BaggingClassifier, BaggingRegressor
from sklearn.ensemble.forest import RandomForestRegressor, RandomForestClassifier
from sklearn.ensemble.gradient_boosting import GradientBoostingClassifier, GradientBoostingRegressor
from sklearn.ensemble.weight_boosting import AdaBoostClassifier, AdaBoostRegressor
from sklearn.linear_model import LogisticRegression, LinearRegression, RidgeClassifier, RidgeCV
from sklearn.metrics.classification import f1_score
from sklearn.metrics.ranking import average_precision_score
from sklearn.metrics.regression import explained_variance_score, r2_score
from sklearn.model_selection import train_test_split
from sklearn.neural_network.multilayer_perceptron import MLPRegressor
from sklearn.svm.classes import LinearSVC, LinearSVR
from sklearn.tree.tree import DecisionTreeClassifier, DecisionTreeRegressor
import sys
import numpy as np

def get_model(fname):
	d_m = None
	with open(fname, "rb") as f:
		d_m = pickle.load(f)
	return d_m


data_file_name = "sme_ipo_test.csv"


log_r = get_model("classifier.joblib")
lin_r = get_model("linear_regression.joblib")
mlp_r = get_model("mlp.joblib")
ridge_r = get_model("ridge.joblib")
svr_r = get_model("svr.joblib")


def get_val(pred_val):
	if pred_val < 0:
		return 100000
	return pred_val


'''
predict_data predicts for single subscription.
'''
def predict_data(dd_rating, gmp, price, subscription, issue_size, is_bullish, name):
	dd_rating_up = dd_rating
	if dd_rating <= 0:
		dd_rating_up = dd_rating - 5
	X = np.array([[dd_rating_up, gmp, price, subscription, issue_size]])
	X_reg = np.array([[dd_rating_up, gmp, price, subscription, issue_size, is_bullish]])
	y_log = log_r.predict(X)
	y_lin = get_val(lin_r.predict(X_reg))
	y_mpl = get_val(mlp_r.predict(X_reg))
	y_ridge = get_val(ridge_r.predict(X_reg))
	y_svr = get_val(svr_r.predict(X_reg))
	#Select the minimum price.
	min_price = min(y_svr, min(y_lin, min(y_mpl, y_ridge)))
	print(f"{name} {y_log[0]}\t{min_price[0]}")


preferred_columns_log = ["dd_rating_up", "gmp", "price", "subscription", "issue_size"]
preferred_columns_lin = ["dd_rating_up", "gmp", "price", "subscription", "issue_size", "is_bullish"]



'''
Read data from data_file_name and load into dataframe.
from the dataframe, extract X_log for columns preferred_columns_log
and extract X_lin for columns preferred_columns_lin.
Do not split test and train. It is only test data.
Predict y_log on X_log using log_r model.
Predict y_price on X_lin using lin_r models.
'''
def predict_data_file():
	sme_data = pd.read_csv(data_file_name)
	df = sme_data
	df['dd_rating_up'] = df['dd_rating']
	df1 = df[df['dd_rating'] <= 0].loc[:]
	df2 = df[df['dd_rating'] > 0].loc[:]
	df1['dd_rating_up'] = df1['dd_rating'] - 5
	df = df1.append(df2)
	df['frac_gmp'] = df['gmp'] - df['price']
	X_log = df.loc[:, preferred_columns_log]
	X_lin = df.loc[:, preferred_columns_lin]
	y_log = log_r.predict(X_log)
	y_lin = lin_r.predict(X_lin)
	y_mpl = mlp_r.predict(X_lin)
	y_ridge = ridge_r.predict(X_lin)
	y_svr = svr_r.predict(X_lin)
	df['pred_status'] = y_log
	df['y_lin'] = y_lin
	df['y_mpl'] = y_mpl
	df['y_ridge'] = y_ridge
	df['y_svr'] = y_svr
	df['pred_open_price'] = df[['y_lin', 'y_mpl', 'y_ridge', 'y_svr']].min(axis=1).round(decimals=2)
	new_df = df[['Issue Name', 'pred_status', 'pred_open_price']]
	new_df.to_csv('output.csv')


predict_data_file()


#Ongoing Data may change
#predict_data(0, 0, 0, 100, 1, 62.52, 0, "Swastik Pipe Limited")
#predict_data(1, 0, 0, 28, 45.74, 7.56, 0, "Cargosol Logistics Limited")
#predict_data(1, 13, 0, 78, 2.59, 21.15, 0, "Silicon Rental Solutions Limited")
#predict_data(0, 0, 0, 40, 6.36, 24, 0, "Lloyds Luxuries Limited")
#predict_data(0, 5, 0, 26, 1, 13.01, 0, "Indong Tea Company Limited")
#predict_data(0, 0, 0, 121, 0.8, 56.87, 0, "QMS Medical Allied Services Ltd")
#predict_data(1, 0, 0, 105, 1.67, 11.71, 0, "Reetech International Cargo and Courier Ltd")
#predict_data(2, 27, 18000, 45, 148.25, 4.86, 0, "Cargotrans Maritime Limited")
#predict_data(1, 27, 55000, 55, 202.41, 8.32, 0, "Concord Control Systems Limited")
#predict_data(0, 27, 13000, 180, 22.31, 14.04, 0, "Cyber Media Research & Services Ltd")
#predict_data(0, 4, 0, 101, 1.34, 35.34, 0, "Trident Lifeline Limited")
#predict_data(0, 0, 0, 60, 0.26, 9.12, 0, "Maagh Advertising And Marketing Services Ltd")
#predict_data(0, 26, 11000, 96, 22.4, 26.02, 0, "Steelman Telecom Limited")
#predict_data(1, 26, 95000, 38, 192.79, 22.6, 0, "Insolation Energy Limited")
#predict_data(1, 46, 92000, 70, 133.43, 30.25, 1, "Annapurna Swadisht Limited")
#predict_data(1, 6, 4000, 122, 5.46, 36.6, 0, "Varanium")
#predict_data(0, 5, 24000, 20, 26.31, 4, 0, "Maks Energy")
#predict_data(0, 2, 0, 30, 1.84, 8.1, 0, "Kandarp Digi Smart")
#predict_data(0, 4, 14000, 15, 121.31, 2.62, 1, "Containe Technologies")
#predict_data(0, 0, 0, 28, 4.94, 3.6, 1, "Mafia Trends Limited(ongoing)")


show_so_far = False
if show_so_far:
	predict_data(0, 1, 6000, 48, 2.62, 5.21, 1, "Tapi Fruit Processing")
	predict_data(1, 6, 45000, 11, 85, 4.48, 1, "Sabar Flex India")
	predict_data(0, 0, 0, 80, 1.62, 18.24, 1, "Ishan International")
	predict_data(1, 24, 40000, 40, 46, 11.4, 1, "Mega Flex Plastics")
	predict_data(0, 0, 0, 81, 2.85, 4.02, 1, "Shantidoot Infra Services")
	predict_data(2, 30, 30000, 55, 85.21, 8.8, 1, "Viviana Power Tech")
	predict_data(1, 0, 78000, 56, 96.91, 30.24, 1, "Virtuoso Optoelectronics")
	predict_data(0, 0, 0, 126, 18.51, 6.35, 1, "EP Biocomposites")
	predict_data(1, 0, 1150, 36, 14.97, 10.8, 1, "Jay Jalaram Technologies")
	predict_data(1, 0, 70000, 34, 251.35, 7.14, 1, "Ameya Precision Engineers")
	predict_data(0, 4, 10000, 30, 20.26, 10.92, 1, "Naturo Indiabull")
	predict_data(0, -4, 0, 70, 1.47, 56, 1, "Rhetan TMT")
	predict_data(0, 3, 6000, 38, 2.77, 15.21, 1, "Dipna Pharmachem")
	predict_data(1, 0, 8500, 61, 8.93, 18.17, 1, "JFL Life Sciences")
	predict_data(0, 0, 43000, 27, 598.82, 1.89, 1, "Olatech Solutions")
	predict_data(0, 0, 65000, 28, 301.47, 4.44, 1, "Veekayem Fashion and Apparels")
	predict_data(1, 0, 18000, 120, 80.5, 22.81, 1, "Upsurge Seeds of Agriculture")
	predict_data(1, 0, 0, 10, 73.66, 5.25, 1, "Agni Green Power")
	predict_data(1, 0.75, 8000, 10, 8.21, 10, 0, "Healthy Life Agritec")
	predict_data(0, 1.5, 0, 27, 1.85, 8.1, 0, "Veerkrupa Jewellers")
	predict_data(1, 5, 7000, 70, 10.06, 30.8, 0, "SKP Bearing")
	predict_data(0, 0, 0, 153, 1.21, 44.36, 0, "B Right Real Estate")
	predict_data(0, 0, 0, 67, 4, 6.19, 0, "Jayant Infratech")
	predict_data(0, 0, 0, 170, 1.77, 15.82, 0, "Kesar India")
	predict_data(1, 0, 0, 101, 1.33, 65.58, 0, "Mangalam Worldwide")
	predict_data(0, 0, 0, 15, 3.24, 1.9, 0, "Sailani Tours N Travels")
	predict_data(0, 0, 0, 186, 1.56, 11.72, 0, "Pearl Green Clubs and Resorts")
	predict_data(1, 0, 0, 30, 5.91, 4.5, 0, "KCK Industries")
	predict_data(1, 0, 8000, 37, 102.89, 13.51, 0, "Fidel Softech")
	predict_data(0, 2, 11000, 29, 14.17, 10.13, 0, "Globesecure Technologies")
	predict_data(0, 9, 9000, 75, 15.69, 48, 0, "Le Merite Exports")
	predict_data(0, 1, 8000, 140, 1.5, 49, 1, "Global Longlife Hospital")
	predict_data(0, 2.5, 7000, 10, 14.75, 6.8, 0, "Fone4 Communications")
	predict_data(1, 45, 0, 150, 2.25, 55.5, 1, "Jeena Sikho Lifecare")
	predict_data(1, 0, 15000, 39, 74.84, 11.89, 1, "Krishna Defence")
	predict_data(1, 0, 19000, 114, 46.34, 31.6, 0, "P E Analytics")
	predict_data(1, -5, 0, 78, 2.18, 40, 1, "Markolines Traffic Controls")
	predict_data(0, 14, 0, 20, 2.36, 4.61, 1, "Navoday Enterprises")

