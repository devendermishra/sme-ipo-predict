
import datetime
import json
from sqlite3 import Error
import bs4
import dateutil.parser

gmp_url = 'https://www.topsharebrokers.com/report/ipo-gmp-performance/377/?sort=gmpd%2Elast%5Fupdateddesc&page='
page_end = 4

'''
tables
	ipo_list (ipo_name, year, level) 0, 1, 2, 3, 4
	ipo_gmp(ipo_name, gmp)
	ipo_basic(ipo_name, ipo_price, ipo_size, issue_size, offer_for_sale, subscription, dd_rating, is_bullish)
'''

'''
connect input_data sqlite3 table 
'''

import sqlite3
import requests

conn = None
db_file = 'input_data'
try:
	conn = sqlite3.connect(db_file)
except Error as e:
	print(e)


def get_nifty100data():
	url = "https://groww.in/v1/api/charting_service/v2/chart/delayed/exchange/NSE/segment/CASH/NIFTY100/5y?intervalInDays=1&minimal=false"

	payload = {}
	headers = {
		'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0',
		'Accept': 'application/json, text/plain, */*',
		'Accept-Language': 'en-US,en;q=0.5',
		'Accept-Encoding': 'gzip, deflate, br',
		'Authorization': '',
		'X-USER-CAMPAIGN': '',
		'x-platform': 'web',
		'X-APP-ID': 'growwWeb',
		'X-DEVICE-ID': '06e05857-2261-5017-8f35-58c726b5deb0',
		'X-REQUEST-CHECKSUM': 'ZTF5eGRyIyMjL2xUNUhqVWZ0bnYyQmVKVkFxMW9rR2drVWtMaGVhWGFmVkw3eU9YVHhsVkF6eURjRjdHWmhjUVJoWXFZNUtNWWJ3OVBUK2dpdDNGMWRWU0t6ZDdVbUdmMnVDVGNhYmt6YlliS0xnZlJIUE09',
		'X-REQUEST-ID': 'fdf9ffb2-701c-49c0-832f-230aa14e13af',
		'Connection': 'keep-alive',
		'Referer': 'https://groww.in/indices/nifty-218500',
		'Sec-Fetch-Dest': 'empty',
		'Sec-Fetch-Mode': 'cors',
		'Sec-Fetch-Site': 'same-origin',
		'Pragma': 'no-cache',
		'Cache-Control': 'no-cache',
		'TE': 'trailers',
		'Cookie': '__cf_bm=KU6lVBWB4_pVwzX09M7Zxu4MptlGfwnQ8oqhytmvlVY-1666021547-0-ATuKIW/iP9fo2XRC2oK0Yv/x/1JQRAzM2XtVpblYA7XSd3MPsibhEwO+XJJuoYod7SrIv3YolaTF2i375MtfJoQ=; __cfruid=08404f32d0595a8f75709d9b8320091a932ddf28-1666021547; _cfuvid=trW6QehWepU4T1B8RyriHdnr_MO49g_xDAlTByRYPyE-1666021547158-0-604800000'
	}

	response = requests.get(url, headers=headers, data=payload)
	data = response.text
	return json.loads(data)


def scrap_is_bullish():
	nifty_data = get_nifty100data()
	candles = nifty_data['candles']
	# candles as [timestamp, value]. Sort it on the basis of timestamp value.
	candles.sort(key=lambda x: x[0])
	print(f"Candles {len(candles)}")
	end_date = get_all_end_dates()
	end_date.sort()
	index = 0
	pindex = 0
	epoch_diff = 31 * 24 * 60 * 60
	for ed in end_date:
		edate = dateutil.parser.parse(ed[0]).timestamp()
		while index < len(candles) and candles[index][0] < edate:
			index = index + 1
		while pindex < index and candles[index - 1][0] - candles[pindex][0] > epoch_diff:
			pindex += 1
		if index < len(candles):
			start_value = candles[index][1]
		else:
			start_value = candles[-1][1]
		end_value = candles[pindex][1]
		diff_value = (end_value - start_value) * 100 / start_value
		is_bullish = 0
		if diff_value > 5:
			is_bullish = 1
		print(f"start value {start_value}  {end_value}  {ed[0]}  {index} {pindex}")
		print(
			f"start time {candles[index][0]} {convert_epoch_to_date(candles[index][0])} {candles[pindex][0]} {convert_epoch_to_date(candles[pindex][0])}  {ed[0]}")
		print("edate " + ed[0] + " is_bullish " + str(is_bullish))
		update_is_bullish(ed[0], is_bullish)


def convert_epoch_to_date(epoch):
	'''
	epoch is unix epoch in seconds. Convert it to string like 2022-12-30
	:param epoch:
	:return: string
	'''
	date_obj = datetime.datetime.fromtimestamp(epoch)
	return date_obj.strftime("%Y-%m-%d")


def scrap_page_url(url):
	headers = {'User-Agent': 'Mozilla/5.0 (X11u; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0', 'Accept': '*/*',
	           'Accept-Language': 'en-US,en;q=0.5'}
	# get the url content using headers
	page = requests.get(url, headers=headers)
	# get the html content
	page_content = page.content
	return page_content


def scrap_ipo_pages():
	url = 'https://www.chittorgarh.com/report/mainboard-ipo-list-in-india-bse-nse/83/?year='
	years = [2022, 2021, 2020]
	for year in years:
		y_url = url + str(year)
		p_content = scrap_page_url(y_url)
		soup = bs4.BeautifulSoup(p_content, 'html.parser')
		selector = 'table-responsive'
		table = soup.find_all(class_=selector)
		tbody = table[1].find_all('tbody')
		trs = tbody[0].find_all('tr')
		for tr in trs:
			tds = tr.select('td')
			td0 = tds[0]
			# select a href from td0
			a_tag = td0.select('a')
			a_href = a_tag[0]['href']
			ipo_name = a_tag[0].text.split('IPO')[0].strip().replace('Ltd.', '').replace('Limited', '').replace('Ltd',
			                                                                                                    '').strip()
			scrap_main_ipo_page(a_href, ipo_name)


alias_name = {'Angel One': 'Angel Broking', 'C.E. Info': 'MapmyIndia', 'UTI Asset': 'UTI AMC',
              'Sona BLW': 'Sona Comstar', 'S.J.S. Enterprises': 'SJS Enterprises', 'S.K.M.S': 'S&K Mutual Fund',
              'S.P.M.S': 'S&P Mutual Fund',
              'Rategain Travel': 'RateGain', 'Rainbow Children\'s': 'Rainbow Childrens Medicare',
              'RailTel Corporation': 'RailTel', 'One 97': 'Paytm', 'PB Fintech': 'Policybazaar',
              'Life Insurance': 'LIC', 'Krishna Institute': 'KIMS',
              'Indian Railway': 'IRFC', 'Home First': 'HFFC', 'FSN E-Commerce': 'Nykaa',
              'Chemcon Speciality': 'Chemcon', 'Computer Age': 'CAMS', 'Easy Trip': 'EaseMyTrip',
              'Equitas Small': 'Equitas SFB'}


def scrap_main_ipo_page(ipo_link, ipo_name):
	short_ipo_name = ' '.join(ipo_name.split(' ')[0:2])
	if short_ipo_name in alias_name.keys():
		short_ipo_name = alias_name[short_ipo_name]
	soup = get_page_bs4(ipo_link)
	trs = soup.find_all('tr')
	ipo_date = None
	issue_size = 0
	ofs_size = 0
	subscription = 0
	for tr in trs:
		txt = tr.get_text().strip()
		if txt.startswith('IPO Date'):
			tds = tr.select('td')
			dts = tds[1].text.split('to')
			if len(dts) >= 2:
				dts = dts[1].strip()
				if len(dts) > 0:
					dt = dateutil.parser.parse(dts)
					ipo_date = dt.date()
		elif txt.startswith('Issue Size'):
			issue_size = get_issue_size(tr)
			if issue_size == '[.]':
				issue_size = 0
			print('Issue Size ', issue_size)
		elif txt.startswith('Offer for Sale'):
			ofs_size = get_issue_size(tr)
			if ofs_size == '[.]':
				ofs_size = 0
			print('Offer for Sale ', ofs_size)
		elif txt.startswith('Total'):
			subscription = tr.select('td')[1].text.strip()
			print('Total subscription', subscription)
	lis = soup.find_all('li')
	dd_rating = 0
	for li in lis:
		txt = li.get_text()
		if txt.startswith('Dilip Davda'):
			ipo_rating = txt.split('-')[1].strip()
			if ipo_rating == 'Subscribe' or ipo_rating == 'Apply':
				dd_rating = 2
			elif ipo_rating == 'May apply':
				dd_rating = 1
			print('Dilip Davda', dd_rating, ipo_rating)
	insert_ipo_basic(short_ipo_name, issue_size, ofs_size, subscription, dd_rating, ipo_date)


def get_issue_size(tr):
	tds = tr.select('td')[1].text.split('up to')[1].strip()
	return tds.replace('₹', '').replace('Cr', '').replace(')', '').replace(',', '').strip()


def get_page_bs4(url):
	page_content = scrap_page_url(url)
	soup = bs4.BeautifulSoup(page_content, 'html.parser')
	return soup


def scrap_gmp_url():
	url = ' https://www.topsharebrokers.com/report/ipo-gmp-performance/377/?sort=gmpd.last_updateddesc&page='
	for i in range(4):
		t_url = url + str(i + 1)
		page_content = scrap_page_url(t_url)
		selector = 'table-responsive'
		'''
		Use beautiful soup to get the selector
		In the selected element, loop over tr where columns
		are 'IPO', 'GMP', 'IPO Price'
		'''
		soup = bs4.BeautifulSoup(page_content, 'html.parser')
		table = soup.find_all(class_=selector)
		tbody = table[0].select('tbody')
		trs = tbody[0].find_all('tr')
		for tr in trs[0:-1]:
			tds = tr.select('td')
			if len(tds) >= 9:
				ipo_name = tds[0].text.strip()
				ipo_name = ipo_name.replace(' IPO', '')
				gmp = tds[1].text.strip().replace('₹', '').split(' ')[0]
				ipo_price = tds[2].text.strip().replace('₹', '')
				ipo_list_price = tds[4].text.strip().split('(')[0]
				listing_date = tds[7].text.strip().split('-')[-1]
				print(ipo_name, gmp, ipo_price, listing_date)
				insert_or_return_ipo_info(ipo_name, listing_date)
				insert_gmp(ipo_name, gmp)
				insert_ipo_price(ipo_name, ipo_price)
				insert_list_price(ipo_name, gmp, ipo_price, ipo_list_price)


def get_all_end_dates():
	# get end_date column distinct from ipo_basic sqlite table.
	# get all the distinct end_date from the table
	sql = 'select distinct end_date from ipo_basic where end_date is not null'
	curs = conn.cursor()
	curs.execute(sql)
	rows = curs.fetchall()
	curs.close()
	return rows


def update_state(ipo_name, level):
	c = conn.cursor()
	c.execute('update ipo_list set level = ? where ipo_name = ?', (level, ipo_name))
	conn.commit()


def insert_or_return_ipo_info(ipo_name, yr, level=0):
	c = conn.cursor()
	res = c.execute('select * from ipo_list where ipo_name=?', (ipo_name,))
	ipo_data = res.fetchone()
	if not ipo_data:
		c.execute("insert into ipo_list values(?, ?, ?)", (ipo_name, yr, level))
		conn.commit()


def insert_gmp(ipo_name, gmp):
	c = conn.cursor()
	res = c.execute('select * from ipo_gmp where ipo_name=?', (ipo_name,))
	ipo_data = res.fetchone()
	if ipo_data:
		c.execute("update ipo_gmp set gmp=? where ipo_name=?", (gmp, ipo_name))
	else:
		c.execute("insert into ipo_gmp values(?, ?)", (ipo_name, gmp))
	conn.commit()
	c.close()


def insert_list_price(ipo_name, gmp, ipo_price, list_price):
	to_subs = float(gmp) + float(ipo_price) <= float(list_price)
	c = conn.cursor()
	res = c.execute('select * from ipo_price where ipo_name=?', (ipo_name,))
	ipo_data = res.fetchone()
	if not ipo_data:
		c.execute('insert into ipo_price values(?, ?, ?, ?)', (ipo_name, list_price, gmp, to_subs))
	else:
		c.execute('update ipo_price set open_price=?, gmp=?, subscribe=? where ipo_name=?',
		          (list_price, gmp, to_subs, ipo_name))
	conn.commit()
	c.close()


def insert_ipo_price(ipo_name, price):
	c = conn.cursor()
	res = c.execute('select * from ipo_basic where ipo_name=?', (ipo_name,))
	ipo_data = res.fetchone()
	if ipo_data:
		c.execute('update ipo_basic set ipo_price=? where ipo_name=?', (price, ipo_name))
	else:
		c.execute('insert into ipo_basic values(?, ?, ?, ?, ?, ?, ?, ?, ?)', (ipo_name, price, 0, 0, 0, 0, 0, 0, 0))
	conn.commit()
	c.close()


def insert_ipo_basic(ipo_name, issue_size, ofs, subscription, dd_rating, end_date):
	c = conn.cursor()
	ofs_prop = 0
	if float(issue_size) > 0:
		ofs_prop = float(ofs) / float(issue_size)
	res = c.execute('select * from ipo_basic where ipo_name like ?', (ipo_name + '%',))
	ipo_data = res.fetchone()
	if ipo_data:
		c.execute(
			'update ipo_basic set issue_size=?, ipo_size=?, offer_for_sale=?, subscription=?, dd_rating=?, end_date=? where ipo_name like ?',
			(issue_size, issue_size, ofs_prop, subscription, dd_rating, end_date, ipo_name + '%'))
	else:
		print("inserting ipo_basic issue size, ofs, subs for " + ipo_name)
		c.execute('insert into ipo_basic values(?, ?, ?, ?, ?, ?, ?, ?, ?)',
		          (ipo_name, 0, issue_size, issue_size, ofs_prop, subscription, dd_rating, 0, end_date))
	conn.commit()
	c.close()


def update_is_bullish(end_date, is_bullish):
	curs = conn.cursor()
	curs.execute('update ipo_basic set is_bullish=? where end_date=?', (is_bullish, end_date))
	conn.commit()
	curs.close()


def output_csv():
	sql = 'select b.ipo_name as name, ipo_price, issue_size, offer_for_sale, subscription, dd_rating, is_bullish, gmp, subscribe, open_price from ipo_basic b, ipo_price p where subscription > 0 and b.ipo_name=p.ipo_name order by b.ipo_name'
	curs = conn.cursor()
	res = curs.execute(sql)
	out_str = 'name,price,issue_size,ofs,subscription,dd_rating,is_bullish,gmp,subscribe,open_price\n'
	for r in res:
		out_str = out_str + ','.join(map(lambda x: str(x).strip(), r)) + '\n'
	print(out_str)


def output_csv_with_date():
	sql = 'select b.ipo_name as name, ipo_price, issue_size, offer_for_sale, subscription, dd_rating, is_bullish, gmp, subscribe, open_price, end_date from ipo_basic b, ipo_price p where subscription > 0 and b.ipo_name=p.ipo_name order by end_date'
	curs = conn.cursor()
	res = curs.execute(sql)
	out_str = 'name,price,issue_size,ofs,subscription,dd_rating,is_bullish,gmp,subscribe,open_price,end_date\n'
	for r in res:
		out_str = out_str + ','.join(map(lambda x: str(x).strip(), r)) + '\n'
	print(out_str)


if __name__ == '__main__':
	scrap_gmp_url()
	scrap_ipo_pages()
	scrap_is_bullish()
	output_csv()
	output_csv_with_date()
