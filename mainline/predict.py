#!/usr/bin/python3

import pickle

import pandas as pd
from sklearn.ensemble.bagging import BaggingClassifier, BaggingRegressor
from sklearn.ensemble.forest import RandomForestRegressor, RandomForestClassifier
from sklearn.ensemble.gradient_boosting import GradientBoostingClassifier, GradientBoostingRegressor
from sklearn.ensemble.weight_boosting import AdaBoostClassifier, AdaBoostRegressor
from sklearn.linear_model import LogisticRegression, LinearRegression, RidgeClassifier, RidgeCV
from sklearn.metrics.classification import f1_score
from sklearn.metrics.ranking import average_precision_score
from sklearn.metrics.regression import explained_variance_score, r2_score
from sklearn.model_selection import train_test_split
from sklearn.neural_network.multilayer_perceptron import MLPRegressor
from sklearn.svm.classes import LinearSVC, LinearSVR
from sklearn.tree.tree import DecisionTreeClassifier, DecisionTreeRegressor
import sys
import numpy as np
import chow_test


def get_model(fname):
	with open(fname, "rb") as f:
		d_m = pickle.load(f)
	return d_m


data_file_name = "ipo_data.csv"

lin_r = get_model("linear_regression.joblib")
ridge_r = get_model("ridge.joblib")


def get_val(pred_val):
	if pred_val < 0:
		return 100000
	return pred_val


preferred_columns_lin = ["dd_rating", "gmp", "price", "subscription", "issue_size", "ofs", "is_bullish"]


'''
Read data from data_file_name and load into dataframe.
from the dataframe, extract X_log for columns preferred_columns_log
and extract X_lin for columns preferred_columns_lin.
Do not split test and train. It is only test data.
Predict y_log on X_log using log_r model.
Predict y_price on X_lin using lin_r models.
'''
def predict_data_file():
	sme_data = pd.read_csv(data_file_name)
	df = sme_data
	X_lin = df.loc[:, preferred_columns_lin]
	y_lin = lin_r.predict(X_lin)
	y_ridge = ridge_r.predict(X_lin)
	df['y_lin'] = y_lin
	df['y_ridge'] = y_ridge
	df['pred_open_price'] = df[['y_lin', 'y_ridge']].min(axis=1).round(decimals=2)
	new_df = df[['name', 'pred_open_price']]
	new_df.to_csv('output.csv')


def chow_test_output():
	ipo_data = pd.read_csv('chow_test_data.csv')
	df = ipo_data
	X_lin = df.loc[:, preferred_columns_lin]
	y_lin = pd.Series(df.open_price)
	significance = 0.10
	print(f"significance {significance}")
	#print(y_lin.__class__)
	#index1 = 12, 72,
	r1 = chow_test.chow_test(X_lin, y_lin, 72, 12, significance)
	print(r1)
	r2 = chow_test.chow_test(X_lin[13:], pd.Series(y_lin[13:]), 93-72, 72-12, significance)
	print(r2)


predict_data_file()
#chow_test_output()
