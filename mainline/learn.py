#!/usr/bin/python3
import pickle

import pandas as pd
from sklearn.ensemble.bagging import BaggingClassifier, BaggingRegressor
from sklearn.ensemble.forest import RandomForestRegressor, RandomForestClassifier
from sklearn.ensemble.gradient_boosting import GradientBoostingClassifier, GradientBoostingRegressor
from sklearn.ensemble.weight_boosting import AdaBoostClassifier, AdaBoostRegressor
from sklearn.linear_model import LogisticRegression, LinearRegression, RidgeClassifier, RidgeCV
from sklearn.metrics.classification import f1_score
from sklearn.metrics.ranking import average_precision_score
from sklearn.metrics.regression import explained_variance_score, r2_score
from sklearn.model_selection import train_test_split
from sklearn.neural_network.multilayer_perceptron import MLPRegressor
from sklearn.svm.classes import LinearSVC, LinearSVR
from sklearn.tree.tree import DecisionTreeClassifier, DecisionTreeRegressor

data_file = "./ipo_data.csv"

sme_data = pd.read_csv(data_file)
df = sme_data
mask1 = df['dd_rating'] <= 0
mask2 = df['dd_rating'] > 0
df['dd_rating_up'] = df['dd_rating']
df1 = df[df['dd_rating'] <= 0].loc[:]
df2 = df[df['dd_rating'] > 0].loc[:]
df1['dd_rating_up'] = df1['dd_rating'] - 5
df = df1.append(df2)

main_columns = ["dd_rating", "price", "gmp", "a_sub", "a_neutral", "a_avoid", "subscription", "issue_size", "is_bse"]


def get_data(columns, output_column, df):
	X = df.loc[:, columns]
	Y = df.loc[:, [output_column]]
	X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25, random_state=50)
	return X_test, X_train, y_test, y_train


def log_reg(X_train, y_train, X_test, y_test, lfunc):
	lfunc.fit(X_train, y_train.values.ravel())
	log_score = lfunc.score(X_test, y_test.values.ravel())
	log_train_score = lfunc.score(X_train, y_train.values.ravel())
	predicted_train = lfunc.predict(X_train)
	predicted_test = lfunc.predict(X_test)
	av_p_train = average_precision_score(y_train.values.ravel(), predicted_train)
	av_p_test = average_precision_score(y_test.values.ravel(), predicted_test)
	f_train = f1_score(y_train.values.ravel(), predicted_train)
	f_test = f1_score(y_test.values.ravel(), predicted_test)
	print(f"\tLogistic test score {log_score}  training score {log_train_score}")
	print(f"\tF1 score train {f_train}  test {f_test} average precision train {av_p_train} test {av_p_test}")



def lin_reg(X_train, y_train, X_test, y_test, lfunc):
	lfunc.fit(X_train, y_train.values.ravel())
	log_score = lfunc.score(X_test, y_test.values.ravel())
	log_train_score = lfunc.score(X_train, y_train.values.ravel())
	print(f"\tLinear test score {log_score}  training score {log_train_score}")
	predicted_train = lfunc.predict(X_train)
	predicted_test = lfunc.predict(X_test)
	ev_train = explained_variance_score(y_train.values.ravel(), predicted_train)
	ev_test = explained_variance_score(y_test.values.ravel(), predicted_test)
	n_test, df = X_test.shape
	n_train, _ = X_train.shape
	df = len(X_test.keys())
	r2_train = r2_score(y_train.values.ravel(), predicted_train)
	r2_test = r2_score(y_test.values.ravel(), predicted_test)
	adj_r2_test = adjusted_r2(r2_test, n_test, df)
	adj_r2_train = adjusted_r2(r2_train, n_train, df)
	print(f"\tESS train {ev_train} test {ev_test} R2 score train {r2_train}  test {r2_test}")
	print(f"\t\tAdjusted R2 train {adj_r2_train}  test {adj_r2_test}")


def adjusted_r2(r2, n, df):
	print(f"\t\t\tr2 {r2}  n {n}  df {df}")
	return 1 - (1-r2)*(n-1)/(n-df-1)


def check_data(df, columns, save_data=False):
	X, X_test, X_train, Y_p, y_test, y_train = get_data(columns, "sub_status", df)
	log_r = LogisticRegression(solver='liblinear', max_iter=1000)
	log_reg(X_train, y_train, X_test, y_test, log_r)
	X_train, X_test, y_train, y_test = train_test_split(X, Y_p, test_size=0.33, random_state=50)
	lin_r = LinearRegression()
	lin_reg(X_train, y_train, X_test, y_test, lin_r)
	if save_data:
		print(lin_r.coef_)
		print(log_r.coef_)
	return log_r, lin_r
'''
Selected:
	Classifier: Linear
	Regressor: RidgeCV, MLPRegressor(max_iter=8000, hidden_layer_sizes=100), LinearSVR(max_iter=20000)
'''

def train_data(df, columns_log, columns_lin, save_data=False):
	X_test, X_train, y_test, y_train = get_data(columns_log, "subscribe", df)
	log_r = LogisticRegression(solver='liblinear', max_iter=1000, random_state=103400)
	print("Classifier - Logistic")
	log_reg(X_train, y_train, X_test, y_test, log_r)
	X_test, X_train, y_test, y_train = get_data(columns_lin, "open_price", df)
	lin_r = LinearRegression()
	ridge_cv = RidgeCV()
	mlp_reg = MLPRegressor(max_iter=16000, hidden_layer_sizes=100, random_state=50)
	svr = LinearSVR(max_iter=400000)
	print("Linear Regression")
	lin_reg(X_train, y_train, X_test, y_test, lin_r)
	print("Ridge Regression")
	lin_reg(X_train, y_train, X_test, y_test, ridge_cv)
	print("MLP Regression")
	lin_reg(X_train, y_train, X_test, y_test, mlp_reg)
	print("SVM Regression")
	lin_reg(X_train, y_train, X_test, y_test, svr)
	if save_data:
		with open("classifier.joblib", "wb") as f:
			pickle.dump(log_r, f)
		with open("linear_regression.joblib", "wb") as f:
			pickle.dump(lin_r, f)
		with open("ridge.joblib", "wb") as f:
			pickle.dump(ridge_cv, f)
		with open("mlp.joblib", "wb") as f:
			pickle.dump(mlp_reg, f)
		with open("svr.joblib", "wb") as f:
			pickle.dump(svr, f)


columns_of_col = [main_columns, ["dd_rating"], ["gmp"], ["a_sub", "a_avoid"], ["subscription"], ["issue_size"],
                  ["is_bse"], ["dd_rating", "gmp", "subscription", "issue_size"],
                  ["dd_rating", "gmp", "price", "subscription", "issue_size"],
                  ["dd_rating", "gmp", "price", "subscription", "issue_size", "a_sub"],]


preferred_columns_log = ["dd_rating_up", "gmp", "price", "subscription", "issue_size", "ofs", "is_bullish"]
preferred_columns_lin = ["dd_rating", "gmp", "price", "subscription", "issue_size", "ofs", "is_bullish"]

write_data = True
train_data(df, preferred_columns_log, preferred_columns_lin, write_data)
